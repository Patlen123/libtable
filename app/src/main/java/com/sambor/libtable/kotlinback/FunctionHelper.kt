package com.sambor.libtable.kotlinback

import android.app.Activity
import android.content.Context
import android.content.res.Configuration
import android.provider.Settings.Global.getString
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.sambor.libtable.R

/**
 * Small class that contains helper functions that need to be used throughout the program
 * I'd prefer to have them global so I used the closest equivalent that Android has to static stuff
 * Because re-writing a check if Android is in dark mode 20,000,000 times doesn't seem very appealing
 */
class FunctionHelper {
    companion object DarkHelp{
        private val mAuth = FirebaseAuth.getInstance()
        private val mDatabase = FirebaseDatabase.getInstance("https://libtable-default-rtdb.europe-west1.firebasedatabase.app")

        var loggedIn = false
        var userData = User("", "", "", arrayListOf())

        //Checks if Android has dark mode enabled
        fun isDarkMode(context: Context): Boolean {
            return when (context.resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK) {
            Configuration.UI_MODE_NIGHT_YES -> true
            else -> false
            }
        }

        //Hides the keyboard, needs to be called from an activity
        fun hideKeyboard(activity: Activity) {
            val imm: InputMethodManager =
                activity.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE) as InputMethodManager
            //Find the currently focused view, so we can grab the correct window token from it.
            var view = activity.currentFocus
            //If no view currently has focus, create a new one, just so we can grab a window token from it
            if (view == null) {
                view = View(activity)
            }
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }

        //Gets user data from the Firebase server and sets them to be the global variables used throughout the program
        //Only takes in a context to know where to show toasts and stuff
        fun getAndSetUserData(context: Context)   {

             val mAuth = FirebaseAuth.getInstance()
             val mDatabase = FirebaseDatabase.getInstance("https://libtable-default-rtdb.europe-west1.firebasedatabase.app")
            /*
                Brief explanation to help me understand mostly
                We're getting the reference to the database, adding a listener for a specific value we want out of the database
                Then we add a listener for that value for each time it changes, I'm not sure why it can't just be done one time
                But whatever it works I think
            */
            mAuth.currentUser!!.uid
            val firebaseData = mDatabase.getReference("Users").child(mAuth.currentUser!!.uid)

            firebaseData.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    val user = snapshot.getValue(User::class.java)
                    if (user != null)   {
                        userData = user
                    }else   {
                        Toast.makeText(context, context.getString(R.string.no_user_data), Toast.LENGTH_LONG).show()
                    }
                }
                override fun onCancelled(error: DatabaseError) {
                    Toast.makeText(context, context.getString(R.string.firebase_no_connect), Toast.LENGTH_LONG).show()

                }
            })
        }


    }
}