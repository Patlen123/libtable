package com.sambor.libtable.kotlinback

import android.Manifest
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.kieronquinn.monetcompat.app.MonetCompatActivity
import com.kieronquinn.monetcompat.core.MonetCompat
import com.sambor.libtable.R
import com.sambor.libtable.kotlinback.FunctionHelper.DarkHelp.loggedIn
import com.sambor.libtable.ui.activities.LoginActivity
import com.sambor.libtable.ui.activities.MainActivity
import com.sambor.libtable.ui.activities.PermissionInfo
import com.vmadalin.easypermissions.EasyPermissions
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.fragment_more.*

/**
 *This class handles a bunch of startup stuff
 * So it signs the user in if they were signed on previously, checks wheteher this is first boot to show location permission screen
 * and tries to show a splash screen but fails most of the time
 */
class StartupHelper : MonetCompatActivity() {
    private val loginAsked = false
    private val mAuth = FirebaseAuth.getInstance()
    private val mDatabase = FirebaseDatabase.getInstance("https://libtable-default-rtdb.europe-west1.firebasedatabase.app")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_permission_helper)
        val sharedPreferences = getSharedPreferences("location", Context.MODE_PRIVATE)

        val loginPreferences = getSharedPreferences("currentuser", Context.MODE_PRIVATE)
        val loginToken = loginPreferences.getString("token", "")

        if (loginToken != null)   {
            if (loginToken.isNotEmpty())  {
                mAuth.signInWithCustomToken(loginToken)
                loggedIn = true
                FunctionHelper.getAndSetUserData(this.applicationContext)
            }
        }

        if ((hasLocationPermission() || sharedPreferences.getBoolean("locationAsked", false)) && mAuth.currentUser != null)    {
            val intent = Intent(applicationContext, MainActivity::class.java)
            startActivity(intent)
        }else if (!sharedPreferences.getBoolean("locationAsked", false))    {
            val intent = Intent(applicationContext, PermissionInfo::class.java)
            startActivity(intent)
        }else   {
            val intent = Intent(applicationContext, LoginActivity::class.java)
            startActivity(intent)
        }


        finish()
    }

    private fun hasLocationPermission(): Boolean {
        return EasyPermissions.hasPermissions(applicationContext, Manifest.permission.ACCESS_FINE_LOCATION)
    }


}