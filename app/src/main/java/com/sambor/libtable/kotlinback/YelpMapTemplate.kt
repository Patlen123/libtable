package com.sambor.libtable.kotlinback

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class YelpMapTemplate (
    @SerializedName("businesses") val restaurants : List<YelpMapRest>

) : Serializable

data class YelpMapRest  (
    @SerializedName("coordinates") val coordinates: YelpCoordinates,
    @SerializedName("id") val id : String,
    @SerializedName("name") val name: String)