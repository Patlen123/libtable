package com.sambor.libtable.kotlinback

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.kieronquinn.monetcompat.core.MonetCompat
import com.sambor.libtable.R
import com.sambor.libtable.kotlinback.FunctionHelper.DarkHelp.isDarkMode
import com.squareup.picasso.Picasso
import java.lang.NumberFormatException

class FavoriteRecyclerAdapter(
    var context: Context,
    var restList: List<YelpRestTemplate>,
    private val mOnSearchListener: OnFavListener
) :
    RecyclerView.Adapter<FavoriteRecyclerAdapter.FavViewHolder>() {
    /**
     * Here we create the RecyclerView and the items
     * @param parent The parent view you're using, in this case it's the SearchFragment
     * @param viewType I don't know what this is but it sure is something
     * @return
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavViewHolder {
        val inflater = LayoutInflater.from(context)
        val view: View = inflater.inflate(R.layout.search_row, parent, false)
        return FavViewHolder(view, mOnSearchListener)
    }

    /**
     * This method binds items declared at the top into the Recycler
     * @param holder The holder for the Recycler items
     * @param position The search position you're looking for in the array
     */
    override fun onBindViewHolder(holder: FavViewHolder, position: Int) {
        holder.crdView.setCardBackgroundColor(
            MonetCompat.getInstance().getBackgroundColor(
                context, isDarkMode(
                    context
                )
            )
        )
        val yelpRestaurant = restList[position]
        holder.txtRestName.text = yelpRestaurant.name
        holder.txtAddress.text =
            yelpRestaurant.location.address1 + ", " + yelpRestaurant.location.postcode
        holder.txtCity.text = yelpRestaurant.location.city
        val testString = yelpRestaurant.image_url
        ratingDisplayer(holder, yelpRestaurant)

        //Picasso doesn't like it if you don't give it an image,
        when {
            testString != "" -> {
                Picasso.get().load(yelpRestaurant.image_url).fit().into(holder.imgLogo)
            }
            isDarkMode(context) -> {
                Picasso.get().load(R.drawable.default_rest_dark).into(holder.imgLogo)
            }
            else -> {
                Picasso.get().load(R.drawable.default_rest_light).into(holder.imgLogo)
            }
        }
    }

    /**
     * Displays the correct image based on the amount of stars,
     * One big if statement so it's a seperate method
     * I'd use a case statement but this is a double so I can't
     */
    fun ratingDisplayer(holder: FavViewHolder, yelpRestaurant: YelpRestTemplate) {
        when (yelpRestaurant.rating) {
            0.0 -> {
                Picasso.get().load(R.drawable.stars_small_0).into(holder.imgRatings)
            }
            1.0 -> {
                Picasso.get().load(R.drawable.stars_small_1).into(holder.imgRatings)
            }
            1.5 -> {
                Picasso.get().load(R.drawable.stars_small_1_half).into(holder.imgRatings)
            }
            2.0 -> {
                Picasso.get().load(R.drawable.stars_small_2).into(holder.imgRatings)
            }
            2.5 -> {
                Picasso.get().load(R.drawable.stars_small_2_half).into(holder.imgRatings)
            }
            3.0 -> {
                Picasso.get().load(R.drawable.stars_small_3).into(holder.imgRatings)
            }
            3.5 -> {
                Picasso.get().load(R.drawable.stars_small_3_half).into(holder.imgRatings)
            }
            4.0 -> {
                Picasso.get().load(R.drawable.stars_small_4).into(holder.imgRatings)
            }
            4.5 -> {
                Picasso.get().load(R.drawable.stars_small_4_half).into(holder.imgRatings)
            }
            5.0 -> {
                Picasso.get().load(R.drawable.stars_small_5).into(holder.imgRatings)
            }
            else -> {
                throw NumberFormatException()
            }
        }
    }

    override fun getItemCount(): Int {
        return restList.size
    }

    /**
     * This method manages the items of the Recycler themselves, so this is where we declare TextViews
     * for example
     */
    inner class FavViewHolder(itemView: View, onSearchListener: OnFavListener?) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var txtRestName: TextView
        var txtCity: TextView
        var txtAddress: TextView
        var imgLogo: ImageView
        var imgRatings: ImageView
        var crdView: CardView
        var onSearchListener: OnFavListener? = null
        override fun onClick(v: View) {
            mOnSearchListener.onFavClick(adapterPosition, restList)
        }

        init {
            var onSearchListener = onSearchListener
            imgRatings = itemView.findViewById(R.id.imgRatings)
            txtRestName = itemView.findViewById(R.id.txtRestName)
            txtCity = itemView.findViewById(R.id.txtCity)
            txtAddress = itemView.findViewById(R.id.txtAddress)
            imgLogo = itemView.findViewById(R.id.imgLogo)
            crdView = itemView.findViewById(R.id.crdView)
            onSearchListener = mOnSearchListener
            itemView.setOnClickListener { v: View ->
                onClick(
                    v
                )
            }
        }
    }

    /**
     * An interface for clicking on an item result, other classes can implement this to switch contexts etc.
     */
    interface OnFavListener {
        fun onFavClick(position: Int, yelpRestaurants: List<YelpRestTemplate>?)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }
}
