package com.sambor.libtable.kotlinback

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * This is what calls the Yelp Fusion API
 */
interface YelpService {
    @GET("businesses/search")
    fun searchRestaurants(@Header("Authorization") authHeader: String,
                          @Query("term") searchTerm : String,
                          @Query("location") location : String,
                          @Query("categories") category: String) : Call<YelpSearchTemplate>

    @GET("businesses/search")
    fun restaurantMapSearch(@Header("Authorization") authHeader: String,
                            @Query("latitude") latitude: Double,
                            @Query("longitude") longitude: Double,
                            @Query("radius") radius:Int,
                            @Query("categories") category: String) : Call<YelpMapTemplate>

    @GET("businesses/{id}")
    fun findRestByID(@Header("Authorization") authHeader: String,
                     @Path("id") restID: String): Call<YelpRestTemplate>

    @GET("businesses/{id}/reviews")
    fun findRestReviews(@Header("Authorization") authHeader:String,
                        @Path("id") revID: String): Call<YelpReviewTemplate>
}
