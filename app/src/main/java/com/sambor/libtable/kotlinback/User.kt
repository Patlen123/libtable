package com.sambor.libtable.kotlinback

import android.media.Image

data class User(
    val id: String = "",
    val name: String = "",
    val email: String = "",
    val favorites: MutableList<String> = arrayListOf()
) {}

data class UserReviews(
    val userId: String = "",
    val reviewId: String = "",
    val title: String = "",
    val body: String = ""
)