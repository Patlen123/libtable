package com.sambor.libtable.kotlinback

import android.net.Uri
import com.google.gson.annotations.SerializedName
import retrofit2.http.Url
import java.io.Serializable
import java.net.URI
import java.net.URL

/**
 * A simple data class for all the things that are contained within the Yelp Fusion API request
 * We have nested lists here, nice
 */
data class YelpSearchTemplate (
    @SerializedName("businesses") val restaurants : List<YelpRestaurants>

    ) : Serializable

data class YelpRestaurants(
    @SerializedName("name") val name: String,
    @SerializedName("location")val location: YelpLocations,
    @SerializedName("coordinates") val coordinates: YelpCoordinates,
    @SerializedName("image_url") val image_url: String,
    @SerializedName("id") val id: String,
    @SerializedName("rating") val rating: Double,
    @SerializedName("review_count") val reviewCount: Int,
    @SerializedName("url") val url: String,
    @SerializedName("phone") val phone: String
): Serializable

data class YelpLocations(
    @SerializedName("city") val city: String,
    @SerializedName("Country") val country: String,
    @SerializedName("address1") val address: String,
    @SerializedName("zip_code") val zip_code: String
): Serializable

data class YelpCoordinates(
    @SerializedName("latitude") val latitude: Double,
    @SerializedName("longitude") val longitude: Double
): Serializable