package com.sambor.libtable.kotlinback

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class YelpRestTemplate (
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String,
    @SerializedName("image_url")val image_url: String,
    @SerializedName("url") val url: String,
    @SerializedName("phone") val phone: String,
    @SerializedName("review_count") val review_count: Int,
    @SerializedName("rating") val rating: Double,
    @SerializedName("location") val location: YelpRestLocation,
    @SerializedName("coordinates") val coordinates: YelpCoordinates,
    @SerializedName("hours") val hours: List<YelpRestOpen>
) : Serializable

data class YelpRestLocation   (
    @SerializedName("address1") val address1: String,
    @SerializedName("city") val city: String,
    @SerializedName("zip_code") val postcode: String,
    @SerializedName("country") val country: String,
)


//Yelp stores these weirdly, so we need a middleman here
data class YelpRestOpen (
    @SerializedName("open") val id: List<YelpRestTimes>
)

data class YelpRestTimes(
    @SerializedName("start") val start: String,
    @SerializedName("end") val end: String,
    @SerializedName("day") val day: Int
)