package com.sambor.libtable.kotlinback

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.gms.tasks.OnFailureListener
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.kieronquinn.monetcompat.core.MonetCompat
import com.sambor.libtable.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_more.*

class LibReviewRowAdapter(
    var context: Context,
    private var revList: List<UserReviews>,
    val mOnLibListener: LibReviewRowAdapter.OnLibListener
    ): RecyclerView.Adapter<LibReviewRowAdapter.LibReviewRowHolder>() {

    private val mStorage = FirebaseStorage.getInstance()
    lateinit var mStorageReference: StorageReference

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LibReviewRowHolder {
        val inflater = LayoutInflater.from(context)
        val view: View = inflater.inflate(R.layout.review_row, parent, false)
        return LibReviewRowHolder(view, mOnLibListener)
    }

    override fun onBindViewHolder(holder: LibReviewRowHolder, position: Int) {
        mStorageReference = mStorage.reference
        holder.crdReviewRow.setCardBackgroundColor(
            MonetCompat.getInstance().getBackgroundColor(
                context, FunctionHelper.isDarkMode(
                    context
                )
            )
        )
        val review = revList[position]

        holder.revRowTitle.text = review.title
        holder.revRowBody.text = review.body

        //An ugly solution to cycle through the 3 image options to find one that works
        //Ideally this'd be a list of images
        //But iterating through a list in mStorage caused me pain last time I tried, so I'm not going to
        mStorageReference.child("images/reviews/${review.reviewId}/image1").downloadUrl.addOnSuccessListener { Uri ->
            Glide.with(this.context).load(Uri).into(holder.revRowImage)
        }.addOnFailureListener( OnFailureListener {
            mStorageReference.child("images/reviews/${review.reviewId}/image2").downloadUrl.addOnSuccessListener { Uri ->
                Glide.with(this.context).load(Uri).into(holder.revRowImage)
            }.addOnFailureListener( OnFailureListener {
                mStorageReference.child("images/reviews/${review.reviewId}/image3").downloadUrl.addOnSuccessListener { Uri ->
                    Glide.with(this.context).load(Uri).into(holder.revRowImage)
                }.addOnFailureListener( OnFailureListener {
                    holder.revRowImage.visibility = View.GONE
                })
            })
        })
    }

    override fun getItemCount(): Int {
        return revList.size
    }

    inner class LibReviewRowHolder(itemView: View, onLibListener: OnLibListener): RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var revRowTitle: TextView
        var revRowBody: TextView
        var crdReviewRow: CardView
        var revRowImage: ImageView
        var mOnLibListener = onLibListener

        override fun onClick(v: View) {
            mOnLibListener.onLibClick(adapterPosition, revList)
        }
        init {
            revRowTitle = itemView.findViewById(R.id.revRowTitle)
            revRowBody = itemView.findViewById(R.id.revRowBody)
            crdReviewRow = itemView.findViewById(R.id.crdReviewRow)
            revRowImage = itemView.findViewById(R.id.recyclerReviewRowImage)
            itemView.setOnClickListener { v: View ->
                onClick(
                    v
                )
            }
        }
    }
    interface OnLibListener {
        fun onLibClick(position: Int, yelpRestaurants: List<UserReviews>?)
    }

}

