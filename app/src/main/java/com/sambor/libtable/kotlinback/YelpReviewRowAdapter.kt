package com.sambor.libtable.kotlinback

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.kieronquinn.monetcompat.core.MonetCompat
import com.sambor.libtable.R
import com.squareup.picasso.Picasso

class YelpReviewRowAdapter(
    var context: Context,
    var revList: List<YelpReviews>): RecyclerView.Adapter<YelpReviewRowAdapter.YelpReviewRowHolder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): YelpReviewRowHolder {
        val inflater = LayoutInflater.from(context)
        val view: View = inflater.inflate(R.layout.review_row, parent, false)
        return YelpReviewRowHolder(view)
    }

    override fun onBindViewHolder(holder: YelpReviewRowHolder, position: Int) {
        holder.crdReviewRow.setCardBackgroundColor(
            MonetCompat.getInstance().getBackgroundColor(
                context, FunctionHelper.isDarkMode(
                    context
                )
            )
        )
        val review = revList[position]

        holder.revRowTitle.text = review.user.name
        holder.revRowBody.text = review.text

        val testString = review.user.imageUrl
                //Picasso doesn't like it if you don't give it an image,
        if (testString != "") {
            Picasso.get().load(review.user.imageUrl).fit().into(holder.revRowImage)
        } else  {
            holder.revRowImage.visibility = View.GONE
        }
    }

    override fun getItemCount(): Int {
        return revList.size
    }

    inner class YelpReviewRowHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        var revRowTitle: TextView
        var revRowBody: TextView
        var crdReviewRow: CardView
        var revRowImage: ImageView
        init {
            revRowTitle = itemView.findViewById(R.id.revRowTitle)
            revRowBody = itemView.findViewById(R.id.revRowBody)
            crdReviewRow = itemView.findViewById(R.id.crdReviewRow)
            revRowImage = itemView.findViewById(R.id.recyclerReviewRowImage)
        }
    }
}

