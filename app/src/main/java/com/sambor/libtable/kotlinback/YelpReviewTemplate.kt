package com.sambor.libtable.kotlinback

import com.google.gson.annotations.SerializedName

data class YelpReviewTemplate(
    @SerializedName("reviews") val reviews: List<YelpReviews>
) {}

data class YelpReviews(
    @SerializedName("rating") val rating: Int,
    @SerializedName("user") val user: YelpUser,
    @SerializedName("text") val text: String,
    @SerializedName("url") val url: String
){}

data class YelpUser(
   @SerializedName("image_url") val imageUrl: String,
   @SerializedName("name") val name: String
){}