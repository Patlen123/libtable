package com.sambor.libtable.ui.activities

import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import com.kieronquinn.monetcompat.app.MonetCompatActivity
import com.kieronquinn.monetcompat.core.MonetCompat
import com.kieronquinn.monetcompat.extensions.views.applyMonetRecursively
import com.kieronquinn.monetcompat.view.MonetToolbar
import com.sambor.libtable.R
import com.sambor.libtable.R.*
import com.squareup.picasso.Picasso
import com.vmadalin.easypermissions.EasyPermissions

/**
 * Class for the permission request activity, uses EasyPermissions to get the permissions because getting them the other way
 * Wasn't the easiest
 */
class PermissionInfo : MonetCompatActivity(), EasyPermissions.PermissionCallbacks {
    private val PERMISSION_REQUEST_CODE = 1

    @SuppressLint("ResourceType")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout.activity_permission_info)

        val wholeView = findViewById<LinearLayout>(R.id.linWhole)
        val imageView = findViewById<ImageView>(R.id.imageView)
        val permToolbar = findViewById<MonetToolbar>(R.id.permToolbar)

        setSupportActionBar(permToolbar)
        supportActionBar!!.title = getString(R.string.permission_title)
        //Dynamic colour setting, MonetRecursively doesn't tend to match Google's design style
        wholeView.applyMonetRecursively()
        wholeView.setBackgroundColor(MonetCompat.getInstance().getBackgroundColor(applicationContext))
        window.statusBarColor = MonetCompat.getInstance().getBackgroundColorSecondary(applicationContext)!!
        permToolbar.setBackgroundColor(MonetCompat.getInstance().getBackgroundColorSecondary(applicationContext)!!)

        Picasso.get().load(drawable.permission_map).fit().into(imageView)
    }


    //Required Override for EasyPermissions
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        // EasyPermissions handles the request result.
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }


    fun onContinue(item: android.view.MenuItem)    {
        EasyPermissions.requestPermissions(this, getString(string.permission_rationale), PERMISSION_REQUEST_CODE, ACCESS_FINE_LOCATION)
    }


    override fun onPermissionsGranted(requestCode: Int, perms: List<String>) {
        //Not technically needed but I'd rather have it here just to be sure nothing goes wrong
        val sharedPreferences = getSharedPreferences("location", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()

        editor.putBoolean("locationAsked", true)
        editor.apply()

        val intent = Intent(applicationContext, LoginActivity::class.java)
        startActivity(intent)
    }

    override fun onPermissionsDenied(requestCode: Int, perms: List<String>) {
        //Storing this for later so the user isn't hassled with the info screen every time they start
        val sharedPreferences = getSharedPreferences("location", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()

        editor.putBoolean("locationAsked", true)
        editor.apply()

        val intent = Intent(applicationContext, LoginActivity::class.java)
        startActivity(intent)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean{
        menuInflater.inflate(R.menu.continue_bar, menu)
        return true
    }

}