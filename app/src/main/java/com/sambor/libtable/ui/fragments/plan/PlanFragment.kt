package com.sambor.libtable.ui.fragments.plan

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.auth.FirebaseAuth
import com.kieronquinn.monetcompat.extensions.views.applyMonetRecursively
import com.sambor.libtable.R
import com.sambor.libtable.databinding.FragmentPlanBinding
import com.sambor.libtable.kotlinback.*
import com.sambor.libtable.ui.activities.MainRestaurant
import com.sambor.libtable.ui.activities.NewReview
import com.vmadalin.easypermissions.EasyPermissions
import kotlinx.android.synthetic.main.fragment_search.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class PlanFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener {

    private lateinit var planViewBinding: PlanViewBinding
    private var _binding: FragmentPlanBinding? = null

    lateinit var mMap: GoogleMap
    lateinit var fusedLocationClient: FusedLocationProviderClient
    lateinit var locRequest: LocationRequest

    lateinit var latLng : LatLng
    lateinit var location : Location

    lateinit var locationList: List<YelpCoordinates>

    private val logCode = "MainActivity"
    private val API_KEY = "Bearer R2_MjmoIB2AM54P8DbanAl_K7mRqdHlsWQXfwSdCiw-uZz2ayrTHKHs0VAnV_3v2Mdj-VUEi9n35A8P_RZAQYCGPC36-yHB73O0aCpxGTJvH47-IYs3EAtIuskKVYXYx"
    private val BASE_URL = "https://api.yelp.com/v3/"

    //Empty hashmap of all markers, used for getting restaurant ID
    private var markerMap: HashMap<Marker, String> = HashMap()

    private val mAuth = FirebaseAuth.getInstance()

    companion object    {
        var mapFragment : SupportMapFragment?=null
    }

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        planViewBinding =
                ViewModelProvider(this).get(PlanViewBinding::class.java)

        _binding = FragmentPlanBinding.inflate(inflater, container, false)
        val root: View = binding.root
        mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment?.getMapAsync(this)

        mAuth.currentUser?.email

        return root
    }

    @SuppressLint("MissingPermission")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.applyMonetRecursively()
    }


    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap) {

        mMap = googleMap

        val cameraUpdate : CameraUpdate = CameraUpdateFactory.newLatLngZoom(LatLng(51.62045, -3.94302), 16F)
        mMap.animateCamera(cameraUpdate)

        if (hasLocationPermission())    {
            mMap.isMyLocationEnabled = true
        }
        mMap.setOnCameraIdleListener { this.onCameraIdle() }
        mMap.setOnInfoWindowClickListener(this)
    }

    private fun onCameraIdle() {
        //Toast.makeText(context, "${mMap.cameraPosition.zoom}", Toast.LENGTH_LONG).show()

        //If you're too far zoomed out then the markers are just confusing
        if (mMap.cameraPosition.zoom < 9)   {
            mMap.clear()
            return
        }


        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this.activity)

        val yelpMapRest: MutableList<YelpMapRest> = arrayListOf()

        //Building the RetroFit calling Service
        val retroFit = Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build()
        val yelpService = retroFit.create(YelpService::class.java)

        /**
         * This block gets the response from the YelpFusion API
         * If it succeeds it fills out the yelpRestaurants list and updates the recycler view with the new values
         */
        yelpService.restaurantMapSearch(API_KEY,mMap.cameraPosition.target.latitude,mMap.cameraPosition.target.longitude, 40000, "restaurants").enqueue(object :
            Callback<YelpMapTemplate> {
            override fun onResponse(call: Call<YelpMapTemplate>, response: Response<YelpMapTemplate>) {
                val body = response.body()
                if (body != null) {
                    yelpMapRest.addAll(body.restaurants)
                    if (yelpMapRest.isEmpty())  {
                        Toast.makeText(context, getString(R.string.map_no_find), Toast.LENGTH_SHORT).show()
                    }
                }

                //The map has performance issues when there's loads of markers, so we clear it if that happens
                if (markerMap.size > 100) {
                    mMap.clear()
                    markerMap.clear()
                }


                for (x in yelpMapRest)  {
                    val marker = mMap.addMarker(MarkerOptions()
                        .position(LatLng(x.coordinates.latitude, x.coordinates.longitude))
                        .title(x.name))
                    markerMap[marker] = x.id


                }

            }
            override fun onFailure(call: Call<YelpMapTemplate>, t: Throwable) {
                val errorText = getString(R.string.yelp_fail_connect) + t
                Toast.makeText(context, errorText, Toast.LENGTH_LONG).show()
            }
        })


    }


    override fun onInfoWindowClick(marker: Marker) {
        if (markerMap.size == 0)  {
            return
        }else   {
            val intent = Intent(context, MainRestaurant::class.java)
            intent.putExtra("id", markerMap.get(marker))

            startActivity(intent)
        }
    }


    /**
     * Checks users current location
     * MissingPermission is supressed because I'm using EasyPermissions to handle that case
     */
    @SuppressLint("MissingPermission")
    private fun fetchLocation() : LatLng? {

        if(!hasLocationPermission()) {
            return null
        }


        return latLng
    }


    private fun hasLocationPermission(): Boolean {
        return EasyPermissions.hasPermissions(context, Manifest.permission.ACCESS_FINE_LOCATION)
    }



    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }




}