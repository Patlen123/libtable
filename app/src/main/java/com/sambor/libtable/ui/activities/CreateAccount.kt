package com.sambor.libtable.ui.activities

import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.graphics.PorterDuff
import android.media.Image
import android.net.Uri
import android.os.Bundle
import android.util.Patterns
import android.view.View
import android.widget.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthException
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.kieronquinn.monetcompat.app.MonetCompatActivity
import com.kieronquinn.monetcompat.core.MonetCompat
import com.kieronquinn.monetcompat.extensions.views.applyMonetRecursively
import com.sambor.libtable.R
import com.sambor.libtable.kotlinback.FunctionHelper
import com.sambor.libtable.kotlinback.FunctionHelper.DarkHelp.loggedIn
import com.sambor.libtable.kotlinback.FunctionHelper.DarkHelp.userData
import com.sambor.libtable.kotlinback.User
import com.sambor.libtable.kotlinback.UserReviews
import com.sambor.libtable.ui.activities.MainActivity.Companion.mainRunning
import kotlinx.android.synthetic.main.activity_create_account.*
import kotlinx.android.synthetic.main.activity_permission_info.view.*

class CreateAccount : MonetCompatActivity() {
    //View Vars
    lateinit var newAccWhole: LinearLayout
    lateinit var newAccName: EditText
    lateinit var newAccEmail: EditText
    lateinit var newAccPassword: EditText
    lateinit var newAccPic: ImageView
    lateinit var newAccProg:ProgressBar

    //Firebase Vars
    lateinit var mAuth: FirebaseAuth
    lateinit var mDatabase: FirebaseDatabase
    lateinit var mStorage: FirebaseStorage
    lateinit var mStorageReference: StorageReference


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_account)
        //Setting our views
        newAccWhole = findViewById(R.id.newAccWhole)
        setSupportActionBar(newAccToolbar)
        newAccName = findViewById(R.id.newAccName)
        newAccEmail = findViewById(R.id.newAccEmail)
        newAccPassword = findViewById(R.id.newAccPass)
        newAccPic = findViewById(R.id.newAccPic)
        newAccProg = findViewById(R.id.newAccProg)
        //Setting Colours
        newAccWhole.applyMonetRecursively()
        newAccWhole.setBackgroundColor(MonetCompat.getInstance().getBackgroundColor(applicationContext, FunctionHelper.isDarkMode(applicationContext)))
        newAccName.background.setColorFilter(MonetCompat.getInstance().getAccentColor(applicationContext, FunctionHelper.isDarkMode(applicationContext)), PorterDuff.Mode.SRC_ATOP)
        newAccEmail.background.setColorFilter(MonetCompat.getInstance().getAccentColor(applicationContext, FunctionHelper.isDarkMode(applicationContext)), PorterDuff.Mode.SRC_ATOP)
        newAccPassword.background.setColorFilter(MonetCompat.getInstance().getAccentColor(applicationContext, FunctionHelper.isDarkMode(applicationContext)), PorterDuff.Mode.SRC_ATOP)
        newAccProg.indeterminateDrawable.setColorFilter(MonetCompat.getInstance().getAccentColor(applicationContext, FunctionHelper.isDarkMode(applicationContext)), PorterDuff.Mode.SRC_IN)

        window.statusBarColor = MonetCompat.getInstance().getBackgroundColorSecondary(applicationContext)!!
        newAccToolbar.setBackgroundColor(MonetCompat.getInstance().getBackgroundColorSecondary(applicationContext)!!)


        supportActionBar!!.title = getString(R.string.new_acc_title)

        //Firebase Stuff
        mAuth = FirebaseAuth.getInstance()
        mDatabase = FirebaseDatabase.getInstance("https://libtable-default-rtdb.europe-west1.firebasedatabase.app")
        mStorage = FirebaseStorage.getInstance()
        mStorageReference = mStorage.reference

        //Setting the tag of the default pic so that there's something to upload even if the user doesn't want a pic there
        val resources = applicationContext.resources
        val resId = R.drawable.ic_baseline_account_circle_200
        newAccPic.tag = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + resources.getResourcePackageName(resId) + "/" + resources.getResourceTypeName(resId) + "/" + resources.getResourceEntryName(resId))

    }

    fun onNewAcc(view: View)    {
        newAccProg.visibility = View.VISIBLE
        val email = newAccEmail.text.toString()
        val name = newAccName.text.toString()
        val password = newAccPassword.text.toString()


        //Validation Checks
        if (email == "" && name == "" && password == "")    {
            newAccEmail.error
            newAccName.error
            newAccPassword.error
            return
        }else if (email== "")    {
            newAccEmail.error = getString(R.string.login_no_email)
            newAccEmail.requestFocus()
            return
        }else if (name== "")   {
            newAccName.error =  getString(R.string.new_acc_no_name)
            newAccName.requestFocus()
            return
        }else if (password== "")   {
            newAccPassword.error = getString(R.string.login_no_password)
            newAccPassword.requestFocus()
            return
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches())   {
            newAccEmail.error = getString(R.string.new_acc_invalid_email)
            newAccEmail.requestFocus()
            return
        }

        if (password.length < 6)    {
            newAccPassword.error =  getString(R.string.new_acc_invalid_password)
            newAccPassword.requestFocus()
            return
        }

        //Signing in and setting data
        mAuth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(this)
            { task ->
                if(task.isSuccessful)   {
                    //Signing in so the user doesn't have to (Also because storing the UID is easier this way :p)
                    mAuth.signInWithEmailAndPassword(email, password)
                    loggedIn = true

                    //Storing the token so the user maintains login status
                    val token = mAuth.currentUser?.getIdToken(true).toString()
                    val sharedPreferences = getSharedPreferences("currentuser", Context.MODE_PRIVATE)
                    val editor = sharedPreferences.edit()
                    editor.putString("token", token)
                    editor.apply()


                    //Sending user info to database, with an empty list of favourites for now
                    val favourites: MutableList<String> = arrayListOf()

                    val user = User(mAuth.currentUser!!.uid, name, email, favourites)
                    userData = user

                    mDatabase.getReference("Users").child(mAuth.currentUser!!.uid).setValue(user).addOnCompleteListener(this){task->
                        if (task.isSuccessful)  {
                            val profPicRef = mStorageReference.child("images/profilepics/${mAuth.currentUser!!.uid}")
                            profPicRef.putFile(Uri.parse(newAccPic.tag.toString()))
                            if (mainRunning)    {
                                finish()
                            }else   {
                                val newIntent = Intent(this, MainActivity::class.java)
                                startActivity(newIntent)
                                finish()
                            }
                        }else   {
                            Toast.makeText(applicationContext, getString(R.string.firebase_no_connect), Toast.LENGTH_LONG).show()
                        }
                    }


                }else   {
                    //Bad way to do this but you know
                    if (task.exception.toString().contains("The email address is already in use"))  {
                        newAccEmail.error =  getString(R.string.new_acc_fail_email)
                        newAccPassword.requestFocus()
                        newAccProg.visibility = View.GONE
                    }else   {
                        val errorText = getString(R.string.new_acc_fail_generic) + "${task.exception}"
                        Toast.makeText(applicationContext, errorText, Toast.LENGTH_LONG).show()
                        newAccProg.visibility = View.GONE
                    }
                }
            }
    }


    //Just ignore that the stuff here is deprecated I couldn't find another way that worked for me thanks
    fun onProfPic(view: View) {
        intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(intent, 1)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)  {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            if(requestCode ==1 && resultCode == RESULT_OK && data.data != null) {
                val imageURI = data.data
                newAccPic.setImageURI(imageURI)
                newAccPic.tag = imageURI
            }
        }
    }
}