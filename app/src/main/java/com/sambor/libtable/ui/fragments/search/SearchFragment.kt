package com.sambor.libtable.ui.fragments.search

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.kieronquinn.monetcompat.core.MonetCompat
import com.kieronquinn.monetcompat.extensions.views.applyMonetRecursively
import com.sambor.libtable.R
import com.sambor.libtable.databinding.FragmentSearchBinding
import com.sambor.libtable.kotlinback.SearchRecyclerAdapter
import com.sambor.libtable.kotlinback.*
import com.sambor.libtable.ui.activities.MainRestaurant
import kotlinx.android.synthetic.main.fragment_search.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class SearchFragment : Fragment(), SearchRecyclerAdapter.OnSearchListener {

    private lateinit var searchViewModel: SearchViewModel
    private var _binding: FragmentSearchBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        searchViewModel =
                ViewModelProvider(this).get(SearchViewModel::class.java)

        _binding = FragmentSearchBinding.inflate(inflater, container, false)




        val root: View = binding.root
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        //Apply Monet Theming to all Views it can default to
        view.applyMonetRecursively()

        super.onViewCreated(view, savedInstanceState)
        //Bind the XML to the code


        //Set Monet BG color
        view.setBackgroundColor(MonetCompat.getInstance().getBackgroundColor(this.requireContext(), FunctionHelper.isDarkMode(this.requireContext())))

        val searchBox = seaTerm
        val recyclerView = searchResults

        searchBox.setBackgroundColor(MonetCompat.getInstance().getPrimaryColor(this.requireContext(), FunctionHelper.isDarkMode(this.requireContext())))

        var yelpRestaurants : MutableList<YelpRestaurants> = arrayListOf()

        val recyclerAdapter = SearchRecyclerAdapter(this.requireContext(), yelpRestaurants, this)

        //recyclerView.adapter = recyclerAdapter
        recyclerView.layoutManager = LinearLayoutManager(this.context)

        recyclerView.addItemDecoration(DividerItemDecoration(this.context, DividerItemDecoration.VERTICAL))
        //SearchViews are weird so I'm making it be an actual box instead of an icon
        searchBox.isIconifiedByDefault = false

        //Whenever user presses search on the keyboard
        searchBox.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String?): Boolean {
                Log.e("onQueryTextChange", "called");
                return false
            }

            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query == null || query == "")   {
                    return false
                }

                //Show the shimmer only after search is pressed
                shimmerFrameLayout.visibility = View.VISIBLE

                getSearch(query, "Swansea")


                return false
            }
        })
    }


    private val logCode = "MainActivity"
    private val API_KEY = "Bearer R2_MjmoIB2AM54P8DbanAl_K7mRqdHlsWQXfwSdCiw-uZz2ayrTHKHs0VAnV_3v2Mdj-VUEi9n35A8P_RZAQYCGPC36-yHB73O0aCpxGTJvH47-IYs3EAtIuskKVYXYx"
    private val BASE_URL = "https://api.yelp.com/v3/"


    fun getSearch(searchTerm: String, location: String)  {


        val recyclerView = searchResults
        var yelpRestaurants : MutableList<YelpRestaurants> = arrayListOf()
        val recyclerAdapter = SearchRecyclerAdapter(this.requireContext(), yelpRestaurants, this)

        recyclerAdapter.setHasStableIds(true)

        recyclerView.adapter = recyclerAdapter
        recyclerView.layoutManager = LinearLayoutManager(this.context)


        //Building the RetroFit calling Service
        val retroFit = Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build()
        val yelpService = retroFit.create(YelpService::class.java)


        /**
         * This block gets the response from the YelpFusion API
         * If it succeeds it fills out the yelpRestaurants list and updates the recycler view with the new values
         */
        yelpService.searchRestaurants(API_KEY,searchTerm, location, "restaurants").enqueue(object :
            Callback<YelpSearchTemplate> {
            override fun onResponse(call: Call<YelpSearchTemplate>, response: Response<YelpSearchTemplate>) {
                val body = response.body()
                if (body != null) {
                    yelpRestaurants.addAll(body.restaurants)

                    if (yelpRestaurants.isEmpty())  {
                        Toast.makeText(context, getString(R.string.search_no_find), Toast.LENGTH_LONG).show()
                    }
                }
                shimmerFrameLayout.visibility = View.GONE
                recyclerView.visibility = View.VISIBLE
                recyclerAdapter.notifyDataSetChanged()
            }

            override fun onFailure(call: Call<YelpSearchTemplate>, t: Throwable) {
                val errorText = getString(R.string.yelp_fail_connect) + t
                Toast.makeText(context, errorText, Toast.LENGTH_LONG).show()
                shimmerFrameLayout.visibility = View.GONE
            }
        })
    }



    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onSearchClick(position: Int, yelpRestaurants: List<YelpRestaurants>?) {
        val yelpRestaurant = yelpRestaurants!![position]
        val intent = Intent(context, MainRestaurant::class.java)
        intent.putExtra("id", yelpRestaurant.id)
        startActivity(intent)
    }
}
