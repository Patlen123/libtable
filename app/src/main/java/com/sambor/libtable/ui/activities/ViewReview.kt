package com.sambor.libtable.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.kieronquinn.monetcompat.app.MonetCompatActivity
import com.kieronquinn.monetcompat.core.MonetCompat
import com.kieronquinn.monetcompat.view.MonetToolbar
import com.sambor.libtable.R
import com.sambor.libtable.kotlinback.FunctionHelper
import com.sambor.libtable.kotlinback.UserReviews
import kotlinx.android.synthetic.main.fragment_my_reviews.*



class ViewReview : MonetCompatActivity() {

    lateinit var viewTitle: TextView
    lateinit var viewBody: TextView
    lateinit var revID: String
    lateinit var viewImage1 : ImageView
    lateinit var viewImage2 : ImageView
    lateinit var viewImage3 : ImageView
    lateinit var viewToolbar: MonetToolbar
    lateinit var viewWholeView: LinearLayout
    lateinit var mAuth: FirebaseAuth
    lateinit var mDatabase: FirebaseDatabase
    lateinit var mStorage: FirebaseStorage
    lateinit var mStorageReference: StorageReference
    lateinit var id: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_review)
        val monetInstance = MonetCompat.getInstance()

        //Setting the views
        viewTitle = findViewById(R.id.viewTitle)
        viewBody = findViewById(R.id.viewBody)
        viewImage1 = findViewById(R.id.viewImage1)
        viewImage2 = findViewById(R.id.viewImage2)
        viewImage3 = findViewById(R.id.viewImage3)
        viewToolbar = findViewById(R.id.viewToolbar)
        viewWholeView = findViewById(R.id.viewWholeView)
        setSupportActionBar(viewToolbar)
        supportActionBar!!.title = getString(R.string.view_title)
        //Setting Colours
        viewWholeView.setBackgroundColor(monetInstance.getBackgroundColor(viewWholeView.context, FunctionHelper.isDarkMode(
            viewWholeView.context)))
        viewToolbar.setBackgroundColor(monetInstance.getBackgroundColorSecondary(applicationContext, FunctionHelper.isDarkMode(applicationContext))!!)

        id = intent.getStringExtra("id").toString()

        val mDatabase = FirebaseDatabase.getInstance("https://libtable-default-rtdb.europe-west1.firebasedatabase.app")
        val mAuth = FirebaseAuth.getInstance()

        val firebaseData = mDatabase.getReference("Reviews/$id")

        firebaseData.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                for (reviewSnapshot in snapshot.children)   {
                        val review = reviewSnapshot.getValue(UserReviews::class.java)
                        viewTitle.hint = review!!.title
                    }

            }
            override fun onCancelled(error: DatabaseError) {
                Toast.makeText(applicationContext, getString(R.string.firebase_no_connect), Toast.LENGTH_LONG).show()
            }
        })

    }

    fun onEdit(item: android.view.MenuItem)  {

    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean{
        menuInflater.inflate(R.menu.edit_bar, menu)
        return true
    }
}