package com.sambor.libtable.ui.fragments.myreviews

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.kieronquinn.monetcompat.core.MonetCompat
import com.kieronquinn.monetcompat.extensions.views.applyMonetRecursively
import com.sambor.libtable.R
import com.sambor.libtable.databinding.FragmentMyReviewsBinding
import com.sambor.libtable.kotlinback.*
import com.sambor.libtable.kotlinback.FunctionHelper.DarkHelp.loggedIn
import com.sambor.libtable.kotlinback.FunctionHelper.DarkHelp.userData
import com.sambor.libtable.ui.activities.LoginActivity
import com.sambor.libtable.ui.activities.MainRestaurant
import com.sambor.libtable.ui.activities.ViewReview
import kotlinx.android.synthetic.main.activity_main_restaurant.*
import kotlinx.android.synthetic.main.fragment_my_reviews.*
import kotlinx.android.synthetic.main.fragment_search.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MyReviewsFragment : Fragment(), View.OnClickListener, FavoriteRecyclerAdapter.OnFavListener, LibReviewRowAdapter.OnLibListener{
    private val monetInstance = MonetCompat.getInstance()
    private lateinit var myReviewsViewModel: MyReviewsViewModel
    private var _binding: FragmentMyReviewsBinding? = null

    private val API_KEY = "Bearer R2_MjmoIB2AM54P8DbanAl_K7mRqdHlsWQXfwSdCiw-uZz2ayrTHKHs0VAnV_3v2Mdj-VUEi9n35A8P_RZAQYCGPC36-yHB73O0aCpxGTJvH47-IYs3EAtIuskKVYXYx"
    private val BASE_URL = "https://api.yelp.com/v3/"

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        myReviewsViewModel =
                ViewModelProvider(this).get(MyReviewsViewModel::class.java)

        _binding = FragmentMyReviewsBinding.inflate(inflater, container, false)
        val root: View = binding.root

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.applyMonetRecursively()
        myReviewLogin.setOnClickListener(this)
        view.setBackgroundColor(monetInstance.getBackgroundColor(this.requireContext(),
            FunctionHelper.isDarkMode(this.requireContext())))
        if (!loggedIn)  {
            favNoLog.visibility = View.VISIBLE
            favLoggedInView.visibility = View.GONE
        }else   {

            /**
             * This block gets the response from the YelpFusion API
             * If it succeeds it fills out the yelpRestaurants list and updates the recycler view with the new values
             */
            val tempUserData = userData

            if (userData.favorites.isEmpty())   {
                favFavouriteView.visibility = View.GONE
            }else   {
                setFavourites()
                setReviews()
            }

        }




    }


    private fun setFavourites() {
        val recyclerView = favouriteRecycler
        var yelpRestaurants : MutableList<YelpRestTemplate> = arrayListOf()
        val recyclerAdapter = FavoriteRecyclerAdapter(this.requireContext(), yelpRestaurants, this)
        recyclerView.addItemDecoration(DividerItemDecoration(this.context, DividerItemDecoration.VERTICAL))
        recyclerAdapter.setHasStableIds(true)

        recyclerView.adapter = recyclerAdapter
        recyclerView.layoutManager = LinearLayoutManager(this.context)


        //Building the RetroFit calling Service
        val retroFit = Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(
            GsonConverterFactory.create()).build()
        val yelpService = retroFit.create(YelpService::class.java)


        for(id in userData.favorites)   {
            yelpService.findRestByID(API_KEY, id).enqueue(object :
                Callback<YelpRestTemplate> {
                override fun onResponse(call: Call<YelpRestTemplate>, response: Response<YelpRestTemplate>) {
                    val body = response.body()
                    if (body != null) {
                        yelpRestaurants.add(response.body()!!)
                    }
                    recyclerView.visibility = View.VISIBLE
                    recyclerAdapter.notifyDataSetChanged()
                    if (yelpRestaurants.isEmpty())  {
                        recyclerView.visibility = View.GONE
                        textView6.visibility = View.GONE
                    }
                }

                override fun onFailure(call: Call<YelpRestTemplate>, t: Throwable) {
                    val errorText = getString(R.string.yelp_fail_connect) + t
                    Toast.makeText(requireContext(), errorText, Toast.LENGTH_LONG).show()
                }
            })
        }
    }

    private fun setReviews()    {
        val recyclerView = reviewRecycler
        var reviewList: MutableList<UserReviews> = arrayListOf()
        val recyclerAdapter = LibReviewRowAdapter(this.requireContext(), reviewList, this)
        recyclerView.addItemDecoration(DividerItemDecoration(this.context, DividerItemDecoration.VERTICAL))
        recyclerAdapter.setHasStableIds(true)

        recyclerView.adapter = recyclerAdapter
        recyclerView.layoutManager = LinearLayoutManager(this.context)

        recyclerView.adapter = recyclerAdapter
        recyclerView.layoutManager = LinearLayoutManager(this.requireContext())
        val mDatabase = FirebaseDatabase.getInstance("https://libtable-default-rtdb.europe-west1.firebasedatabase.app")
        val mAuth = FirebaseAuth.getInstance()

        val firebaseData = mDatabase.getReference("Reviews")

        firebaseData.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                for (reviewSnapshot in snapshot.children)   {
                    for (reviewData in reviewSnapshot.children) {
                        val review = reviewData.getValue(UserReviews::class.java)

                        if (review != null) {
                            if (review.userId == userData.id) {
                                Log.i("restTest", mAuth.currentUser!!.uid)
                                Log.i("restTest", review.toString())
                                reviewList.add(review)

                            }
                        }
                    }
                }

                if (reviewList.isNotEmpty())   {
                    recyclerAdapter.notifyDataSetChanged()
                }else   {
                    recyclerView.visibility = View.GONE
                    textView1000.visibility = View.GONE
                }
            }
            override fun onCancelled(error: DatabaseError) {
                Toast.makeText(context, getString(R.string.firebase_no_connect), Toast.LENGTH_LONG).show()

            }
        })
    }

    override fun onResume() {
        super.onResume()
        if (loggedIn)  {
            favNoLog.visibility = View.GONE
            favMain.visibility = View.VISIBLE
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    /*Personal note here that I really don't like fragments
    And Android Studio's autocomplete can go die in a fire
    Why wouldn't it figure out that I mean OnClickListener when I type in onClickListener???????*/
    override fun onClick(p0: View?) {
        val intent = Intent(context, LoginActivity::class.java)
        startActivity(intent)
    }

    override fun onFavClick(position: Int, yelpRestaurants: List<YelpRestTemplate>?) {
        val yelpRestaurant = yelpRestaurants!![position]
        val intent = Intent(context, MainRestaurant::class.java)
        intent.putExtra("id", yelpRestaurant.id)
        startActivity(intent)
    }

    override fun onLibClick(position: Int, yelpRestaurants: List<UserReviews>?) {
        val yelpRestaurant = yelpRestaurants!![position]
        val intent = Intent(context, ViewReview::class.java)
        intent.putExtra("id", yelpRestaurant.reviewId)
        startActivity(intent)
    }
}