package com.sambor.libtable.ui.activities

import android.content.Intent
import android.graphics.PorterDuff
import android.net.Uri
import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.util.Log
import android.view.Menu
import android.view.View
import android.widget.*
import androidx.core.net.toUri
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.kieronquinn.monetcompat.app.MonetCompatActivity
import com.kieronquinn.monetcompat.core.MonetCompat
import com.kieronquinn.monetcompat.extensions.views.applyMonetRecursively
import com.kieronquinn.monetcompat.extensions.views.setTint
import com.sambor.libtable.R
import com.sambor.libtable.kotlinback.*
import com.sambor.libtable.kotlinback.FunctionHelper.DarkHelp.loggedIn
import com.sambor.libtable.kotlinback.FunctionHelper.DarkHelp.userData
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main_restaurant.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.NumberFormatException
import java.time.DayOfWeek

class MainRestaurant : MonetCompatActivity(), LibReviewRowAdapter.OnLibListener {



    var theYelpRestaurant: YelpRestTemplate? = null
    private val logCode = "MainActivity"
    private val API_KEY = "Bearer R2_MjmoIB2AM54P8DbanAl_K7mRqdHlsWQXfwSdCiw-uZz2ayrTHKHs0VAnV_3v2Mdj-VUEi9n35A8P_RZAQYCGPC36-yHB73O0aCpxGTJvH47-IYs3EAtIuskKVYXYx"
    private val BASE_URL = "https://api.yelp.com/v3/"

    lateinit var restImage: ImageView
    lateinit var restRatings: ImageView
    lateinit var restRevCount: TextView
    lateinit var restToolbar: com.kieronquinn.monetcompat.view.MonetToolbar
    lateinit var restThrob: ProgressBar
    lateinit var nestView: NestedScrollView

    lateinit var restStreet: TextView
    lateinit var restCity: TextView
    lateinit var restCountry: TextView
    lateinit var restPostcode: TextView

    lateinit var restOpenTitle: TextView

    lateinit var restTableHours: TableLayout

    lateinit var newReview: Button
    lateinit var restFavAction: FloatingActionButton

    lateinit var restMonday: TextView
    lateinit var restTuesday: TextView
    lateinit var restWednesday: TextView
    lateinit var restThursday: TextView
    lateinit var restFriday: TextView
    lateinit var restSaturday: TextView
    lateinit var restSunday: TextView



    lateinit var id: String

    val mAuth: FirebaseAuth = FirebaseAuth.getInstance()
    val mDatabase = FirebaseDatabase.getInstance("https://libtable-default-rtdb.europe-west1.firebasedatabase.app")

    var restFav = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_restaurant)
        val monetInstance = MonetCompat.getInstance()


        //Setting all the views
        nestView = findViewById<NestedScrollView>(R.id.nestRestMainWhole)
        restImage = findViewById<ImageView>(R.id.imgRestImage)
        restRatings = findViewById<ImageView>(R.id.imgRestRatings)
        restRevCount = findViewById<TextView>(R.id.txtRestRevCount)
        restToolbar = findViewById<com.kieronquinn.monetcompat.view.MonetToolbar>(R.id.restToolbar)
        restThrob = findViewById<ProgressBar>(R.id.restThrob)
        val wholeView = findViewById<LinearLayout>(R.id.restLinWhole)

        restStreet = findViewById<TextView>(R.id.restStreet)
        restCity = findViewById<TextView>(R.id.restCity)
        restPostcode = findViewById<TextView>(R.id.restPostcode)
        restCountry = findViewById<TextView>(R.id.restCountry)

        restMonday = findViewById<TextView>(R.id.restMonday)
        restTuesday = findViewById<TextView>(R.id.restTuesday)
        restWednesday = findViewById<TextView>(R.id.restWednesday)
        restThursday = findViewById<TextView>(R.id.restThursday)
        restFriday = findViewById<TextView>(R.id.restFriday)
        restSaturday = findViewById<TextView>(R.id.restSaturday)
        restSunday = findViewById<TextView>(R.id.restSunday)


        restTableHours = findViewById(R.id.tableHours)

        restOpenTitle = findViewById(R.id.restOpenTitle)


        newReview = findViewById(R.id.btnRestNewRev)
        restFavAction = findViewById(R.id.restFavAction)

        restThrob.visibility = View.VISIBLE
        nestView.visibility = View.GONE
        
        //Setting colours
        nestView.applyMonetRecursively()
        nestView.setBackgroundColor(monetInstance.getBackgroundColor(applicationContext))
        window.
        statusBarColor =
            monetInstance.getBackgroundColorSecondary(applicationContext, FunctionHelper.isDarkMode(applicationContext))!!
        restFavAction.setTint(monetInstance.getAccentColor(applicationContext, FunctionHelper.isDarkMode(applicationContext)))
        restToolbar.setBackgroundColor(monetInstance.getBackgroundColorSecondary(applicationContext, FunctionHelper.isDarkMode(applicationContext))!!)
        wholeView.setBackgroundColor(monetInstance.getBackgroundColor(applicationContext))
        restThrob.indeterminateDrawable.setColorFilter(monetInstance.getAccentColor(applicationContext, FunctionHelper.isDarkMode(applicationContext)), PorterDuff.Mode.SRC_IN)

        setSupportActionBar(restToolbar)
        supportActionBar!!.setTitle(R.string.rest_loading)
        id = intent.getStringExtra("id").toString()
        if (id == "") {
            Toast.makeText(applicationContext, "We didn't detect an ID for this restaurant, returning to previous view", Toast.LENGTH_LONG).show()
            finish()
        }
        getRestInfo(id!!)

        if (userData.favorites.contains(id))    {
            restFavAction.setImageDrawable(getDrawable(R.drawable.ic_baseline_star_24))
            restFav = true
        }else   {
            restFavAction.setImageDrawable(getDrawable(R.drawable.nav_review_24))
            restFav = false
        }

    }

    fun getRestInfo(id: String)   {
        //Building the RetroFit calling Service
        val retroFit = Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build()
        val yelpService = retroFit.create(YelpService::class.java)


        /**
         * This block gets the response from the YelpFusion API
         * If it succeeds it fills out the yelpRestaurants list and updates the recycler view with the new values
         */
        yelpService.findRestByID(API_KEY, id).enqueue(object :
            Callback<YelpRestTemplate> {
            override fun onResponse(call: Call<YelpRestTemplate>, response: Response<YelpRestTemplate>) {
                val body = response.body()
                theYelpRestaurant = response.body()
                setUI()
            }

            override fun onFailure(call: Call<YelpRestTemplate>, t: Throwable) {
                val errorText = getString(R.string.yelp_fail_connect) + t
                Toast.makeText(applicationContext, errorText, Toast.LENGTH_LONG).show()
            }
        })
    }

    //After all the data is gotten from the Yelp API, we need to set all the various values to be correct
    fun setUI() {
        supportActionBar!!.title = theYelpRestaurant!!.name

        ratingDisplayer(restRatings, theYelpRestaurant!!)

        setSupportActionBar(restToolbar)
        supportActionBar!!.title = theYelpRestaurant!!.name

        //Setting some custom text for the review count
        if (theYelpRestaurant!!.review_count == 1)    {
            restRevCount.text = "${getString(R.string.yelp_based)} ${theYelpRestaurant!!.review_count} ${getString(R.string.yelp_1_review)}"
        }else   {
            restRevCount.text = "${getString(R.string.yelp_based)} ${theYelpRestaurant!!.review_count} ${getString(R.string.yelp_2_reviews)}"
        }


        restStreet.text = theYelpRestaurant!!.location.address1
        restCity.text = theYelpRestaurant!!.location.city
        restCountry.text = theYelpRestaurant!!.location.country
        restPostcode.text = theYelpRestaurant!!.location.postcode

        if (theYelpRestaurant!!.hours == null)    {
            restOpenTitle.visibility = View.GONE
            restTableHours.visibility = View.GONE
        }else   {
            var hoursCount = 0
            for (hours in theYelpRestaurant!!.hours[0].id)    {
                //This solution really really sucks and is ugly as sin and I know that but I'm out of ideas for how to handle this properly
                when (hoursCount)   {
                    0 ->
                        restMonday.text = "${hours.start} - ${hours.end}"
                    1 ->
                        restTuesday.text = "${hours.start} - ${hours.end}"
                    2 ->
                        restWednesday.text = "${hours.start} - ${hours.end}"
                    3 ->
                        restThursday.text = "${hours.start} - ${hours.end}"
                    4 ->
                        restFriday.text = "${hours.start} - ${hours.end}"
                    5 ->
                        restSaturday.text = "${hours.start} - ${hours.end}"
                    6 ->
                        restSunday.text = "${hours.start} - ${hours.end}"
                }
                hoursCount +=1
            }
        }



        if(theYelpRestaurant!!.image_url.equals("") && FunctionHelper.isDarkMode(this.applicationContext)) {
            Picasso.get().load(R.drawable.default_rest_dark).into(restImage)
        }else if (theYelpRestaurant!!.image_url.equals("") && !FunctionHelper.isDarkMode(this.applicationContext))   {
            Picasso.get().load(R.drawable.default_rest_light).into(restImage)
        }else   {
            Picasso.get().load(theYelpRestaurant!!.image_url).into(restImage)
        }




        restThrob.visibility = View.GONE
        nestView.visibility = View.VISIBLE

        if (!loggedIn) {
            btnRestNewRev.visibility = View.GONE
            restFavAction.visibility = View.GONE
        }

        getYelpReviews()
        getLibReviews()
    }

    //Gets the review excerpts from the Yelp API, if it finds nothing then the view is hidden
    private fun getYelpReviews()    {
        val recyclerView = yelpRecycler
        var yelpReviews : MutableList<YelpReviews> = arrayListOf()
        val recyclerAdapter = YelpReviewRowAdapter(nestView.context, yelpReviews)

        recyclerAdapter.setHasStableIds(true)

        recyclerView.adapter = recyclerAdapter
        recyclerView.layoutManager = LinearLayoutManager(applicationContext)

        recyclerView.addItemDecoration(DividerItemDecoration(nestView.context, DividerItemDecoration.VERTICAL))

        //Building the RetroFit calling Service
        val retroFit = Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build()
        val yelpService = retroFit.create(YelpService::class.java)


        /**
         * This block gets the response from the YelpFusion API
         * If it succeeds it fills out the yelpRestaurants list and updates the recycler view with the new values
         */
        yelpService.findRestReviews(API_KEY,id).enqueue(object :
            Callback<YelpReviewTemplate> {
            override fun onResponse(call: Call<YelpReviewTemplate>, response: Response<YelpReviewTemplate>) {
                Log.i(logCode, "Epic $response")
                val body = response.body()
                if (body != null) {
                    yelpReviews.addAll(body.reviews)

                    if (yelpReviews.isNotEmpty())  {
                        linYelpReviews.visibility = View.VISIBLE
                    }
                }
                recyclerAdapter.notifyDataSetChanged()
            }

            override fun onFailure(call: Call<YelpReviewTemplate>, t: Throwable) {
                val errorText = getString(R.string.yelp_fail_connect) + t
                Toast.makeText(applicationContext, errorText, Toast.LENGTH_LONG).show()
            }
        })
    }

    private fun getLibReviews()    {
        val recyclerView = libRecycler
        var listOfReviews : MutableList<UserReviews> = arrayListOf()
        val recyclerAdapter = LibReviewRowAdapter(nestView.context, listOfReviews, this)

        recyclerAdapter.setHasStableIds(true)

        recyclerView.addItemDecoration(DividerItemDecoration(nestView.context, DividerItemDecoration.VERTICAL))

        recyclerView.adapter = recyclerAdapter
        recyclerView.layoutManager = LinearLayoutManager(applicationContext)
        val mDatabase = FirebaseDatabase.getInstance("https://libtable-default-rtdb.europe-west1.firebasedatabase.app")
        val mAuth = FirebaseAuth.getInstance()

        val firebaseData = mDatabase.getReference("Reviews").child(id)

        firebaseData.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                for (reviewSnapshot in snapshot.children)   {
                    val review = reviewSnapshot.getValue(UserReviews::class.java)
                    if (review != null) {
                        listOfReviews.add(review)
                    }
                }

                if (listOfReviews.isNotEmpty())   {
                    linLibReviews.visibility = View.VISIBLE
                    recyclerAdapter.notifyDataSetChanged()
                }else   {

                }
            }
            override fun onCancelled(error: DatabaseError) {
                Toast.makeText(applicationContext, getString(R.string.firebase_no_connect), Toast.LENGTH_LONG).show()

            }
        })

    }

    //The only time within the app that this resumes is after a review is made so this is what we doing now
    override fun onResume() {
        super.onResume()
        getLibReviews()
    }



    //Menu methods
    override fun onCreateOptionsMenu(menu : Menu): Boolean {
        menuInflater.inflate(R.menu.main_restaurant_bar, menu)
        return true
    }
    fun onMap(item: android.view.MenuItem) {
        val coordinates = theYelpRestaurant!!.coordinates
    }
    fun onCall(item: android.view.MenuItem) {
        val phoneNumber = "tel:" + theYelpRestaurant!!.phone
        val callIntent: Intent = Intent(Intent.ACTION_DIAL).setData(Uri.parse(phoneNumber))
        startActivity(callIntent)
    }
    fun onWebsite(item: android.view.MenuItem) {
        val theUri: Uri? = theYelpRestaurant!!.url.toUri()  //It won't be null because it's initialised before you can even click
        val browserIntent: Intent = Intent(Intent.ACTION_VIEW).setData(theUri)
        startActivity(browserIntent)
    }

    fun onReview(view: View)    {
        val intent = Intent(this.applicationContext, NewReview::class.java)
        intent.putExtra("id", theYelpRestaurant!!.id)
        intent.putExtra("name", theYelpRestaurant!!.name)
        startActivity(intent)
    }

    fun onFavAction(view:View)  {
        if (restFav)    {
            restFavAction.setImageDrawable(getDrawable(R.drawable.nav_review_24))
            restFav = false
            FunctionHelper.userData.favorites.remove(id)
            mDatabase.getReference("Users").child(mAuth.currentUser!!.uid).setValue(userData).addOnCompleteListener(this){task->
                if (task.isSuccessful)  {
                    Toast.makeText(applicationContext, getString(R.string.rest_fav_removed), Toast.LENGTH_SHORT).show()
                }else   {
                    Toast.makeText(applicationContext, getString(R.string.firebase_no_connect), Toast.LENGTH_LONG).show()
                }
            }
        }else   {
            restFavAction.setImageDrawable(getDrawable(R.drawable.ic_baseline_star_24))
            restFav = true
            FunctionHelper.userData.favorites.add(id)
            mDatabase.getReference("Users").child(mAuth.currentUser!!.uid).setValue(userData).addOnCompleteListener(this){task->
                if (task.isSuccessful)  {
                    Toast.makeText(applicationContext, getString(R.string.rest_fav_added), Toast.LENGTH_SHORT).show()
                }else   {
                    Toast.makeText(applicationContext, getString(R.string.firebase_no_connect), Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    /**
     * This was gonna be a case statement but that doesn't like floats so it's this instead
     * Also ugly as sin but \_O_/
     */
    fun ratingDisplayer(imageView: ImageView, yelpRestaurant: YelpRestTemplate) {
        when (yelpRestaurant.rating) {
            0.0 -> {
                Picasso.get().load(R.drawable.stars_large_0).into(imageView)
            }
            1.0 -> {
                Picasso.get().load(R.drawable.stars_large_1).into(imageView)
            }
            1.5 -> {
                Picasso.get().load(R.drawable.stars_large_1_half).into(imageView)
            }
            2.0 -> {
                Picasso.get().load(R.drawable.stars_large_2).into(imageView)
            }
            2.5 -> {
                Picasso.get().load(R.drawable.stars_large_2_half).into(imageView)
            }
            3.0 -> {
                Picasso.get().load(R.drawable.stars_large_3).into(imageView)
            }
            3.5 -> {
                Picasso.get().load(R.drawable.stars_large_3_half).into(imageView)
            }
            4.0 -> {
                Picasso.get().load(R.drawable.stars_large_4).into(imageView)
            }
            4.5 -> {
                Picasso.get().load(R.drawable.stars_large_4_half).into(imageView)
            }
            5.0 -> {
                Picasso.get().load(R.drawable.stars_large_5).into(imageView)
            }
            else -> {
                throw NumberFormatException()
            }
        }
    }

    override fun onLibClick(position: Int, yelpRestaurants: List<UserReviews>?) {
        val yelpRestaurant = yelpRestaurants!![position]
        val intent = Intent(nestView.context, ViewReview::class.java)
        intent.putExtra("id", yelpRestaurant.reviewId)
        startActivity(intent)
    }
}