package com.sambor.libtable.ui.fragments.more

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageException
import com.google.firebase.storage.StorageReference
import com.kieronquinn.monetcompat.core.MonetCompat
import com.kieronquinn.monetcompat.extensions.views.applyMonetRecursively
import com.sambor.libtable.R
import com.sambor.libtable.databinding.FragmentMoreBinding
import com.sambor.libtable.kotlinback.FunctionHelper
import com.sambor.libtable.kotlinback.FunctionHelper.DarkHelp.loggedIn
import com.sambor.libtable.kotlinback.FunctionHelper.DarkHelp.userData
import com.sambor.libtable.ui.activities.LoginActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_more.*

class MoreFragment : Fragment() {

    private lateinit var moreViewModel: MoreViewModel
    private var _binding: FragmentMoreBinding? = null


    private var mAuth = FirebaseAuth.getInstance()
    //18:55PM the time I realised after 3 hours that I forgot to add the URL to FirebaseDatabse and that's why I couldn't pull a value
    //The date of this timestamp is unimportant
    private var mDatabase = FirebaseDatabase.getInstance("https://libtable-default-rtdb.europe-west1.firebasedatabase.app")
    private val mStorage = FirebaseStorage.getInstance()
    lateinit var mStorageReference: StorageReference

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        moreViewModel =
                ViewModelProvider(this).get(MoreViewModel::class.java)

        _binding = FragmentMoreBinding.inflate(inflater, container, false)
        val root: View = binding.root




        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val monetInstance = MonetCompat.getInstance()
        view.applyMonetRecursively()
        moreTitle.setBackgroundColor(
            MonetCompat.getInstance().getBackgroundColorSecondary(
                this.requireContext(),
                FunctionHelper.isDarkMode(this.requireContext())
            )!!
        )

        mStorageReference = mStorage.reference

        view.setBackgroundColor(monetInstance.getBackgroundColor(this.requireContext(),
            FunctionHelper.isDarkMode(this.requireContext())))


        if (!loggedIn) {
            moreLoginText.text = getString(R.string.not_logged_in)
            moreLoggedOutView.visibility = View.VISIBLE
            moreLoggedInView.visibility = View.GONE
        } else {
            moreLoggedInView.visibility = View.VISIBLE
            moreLoggedOutView.visibility = View.GONE
            val loginText = getString(R.string.logged_in_as)
            moreLoginText.text = loginText + " ${FunctionHelper.userData.name}"
            mStorageReference.child("images/profilepics/${userData.id}").downloadUrl.addOnSuccessListener { Uri ->
                Glide.with(requireContext()).load(Uri).into(moreProfPic)
            }
        }




        moreLogoutBut.setOnClickListener(View.OnClickListener { view -> onLogout(view)})
        moreProfPic.setOnClickListener(View.OnClickListener { view-> onProfPic(view) })
        moreButtonLogin.setOnClickListener(View.OnClickListener { view -> onLogin(view) })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun onLogout(view: View)    {
        mAuth.signOut()
        moreLoginText.text = getString(R.string.not_logged_in)
        moreLoggedInView.visibility = View.GONE
        moreLoggedOutView.visibility = View.VISIBLE

        val token = mAuth.currentUser?.getIdToken(true).toString()
        val sharedPreferences = this.requireActivity().getSharedPreferences("currentuser", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.remove("token")
        editor.apply()


        loggedIn = false
    }

    private fun onProfPic(view: View) {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(intent, 1)
    }

    private fun onLogin(view: View) {
        val intent = Intent(context, LoginActivity::class.java)
        startActivity(intent)
    }

    override fun onResume() {
        super.onResume()
        if (loggedIn) {
            moreLoggedInView.visibility = View.VISIBLE
            moreLoggedOutView.visibility = View.GONE
            val loginText = getString(R.string.logged_in_as)
            moreLoginText.text = loginText + " ${FunctionHelper.userData.name}"
            mStorageReference.child("images/profilepics/${userData.id}").downloadUrl.addOnSuccessListener { Uri ->
                Glide.with(requireContext()).load(Uri).into(moreProfPic)
                mStorageReference.child("images/profilepics/${userData.id}").downloadUrl.addOnSuccessListener { Uri ->
                    Glide.with(requireContext()).load(Uri).into(moreProfPic)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)  {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            if(requestCode ==1 && resultCode == AppCompatActivity.RESULT_OK && data.data != null) {
                val imageURI = data.data
                moreProfPic.setImageURI(imageURI)
                moreProfPic.tag = imageURI
            }
        }
    }
}