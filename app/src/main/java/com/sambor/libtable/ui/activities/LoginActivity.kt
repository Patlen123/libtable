package com.sambor.libtable.ui.activities

import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.widget.LinearLayout
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import com.kieronquinn.monetcompat.app.MonetCompatActivity
import com.kieronquinn.monetcompat.extensions.views.applyMonetRecursively
import com.sambor.libtable.R
import android.content.Context
import android.content.Intent
import android.graphics.PorterDuff
import android.view.Menu
import com.google.android.material.button.MaterialButton
import com.kieronquinn.monetcompat.core.MonetCompat
import com.kieronquinn.monetcompat.view.MonetToolbar
import com.sambor.libtable.kotlinback.FunctionHelper
import com.sambor.libtable.kotlinback.FunctionHelper.DarkHelp.loggedIn


class LoginActivity : MonetCompatActivity() {
    private var mAuth = FirebaseAuth.getInstance()
    private lateinit var loginEmail : TextInputEditText
    private lateinit var loginPassword: TextInputEditText
    private lateinit var loginToolbar: MonetToolbar
    private lateinit var loginButton: MaterialButton


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        //Getting Views
        val loginWhole = findViewById<LinearLayout>(R.id.loginWhole)
        loginEmail = findViewById(R.id.loginEmail)
        loginPassword = findViewById(R.id.loginPassword)
        loginToolbar = findViewById(R.id.loginToolbar)
        loginButton = findViewById(R.id.loginButton)

        //Setting Colours
        loginWhole.applyMonetRecursively()
        loginWhole.setBackgroundColor(MonetCompat.getInstance().getBackgroundColor(applicationContext))
        setSupportActionBar(loginToolbar)
        loginToolbar.setBackgroundColor(MonetCompat.getInstance().getBackgroundColorSecondary(applicationContext)!!)
        supportActionBar!!.title = getString(R.string.login_text)
        loginEmail.background.setColorFilter(MonetCompat.getInstance().getAccentColor(applicationContext, FunctionHelper.isDarkMode(applicationContext)), PorterDuff.Mode.SRC_ATOP)
        loginPassword.background.setColorFilter(MonetCompat.getInstance().getAccentColor(applicationContext, FunctionHelper.isDarkMode(applicationContext)), PorterDuff.Mode.SRC_ATOP)



        window.statusBarColor = MonetCompat.getInstance().getBackgroundColorSecondary(applicationContext)!!
        //Makes it so that when you press enter on the keyboard it does onLogin
        loginPassword.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent): Boolean {
                // if the event is a key down event on the enter button
                if (event.action == KeyEvent.ACTION_DOWN &&
                    keyCode == KeyEvent.KEYCODE_ENTER
                ) {
                    onLogin(loginWhole)
                    return true
                }
                return false
            }
        })
    }

    fun onLogin(view: View)    {
        //Validation Checks
        if (loginEmail.text.toString() == "" && loginPassword.text.toString() == "")    {
            loginEmail.error = getString(R.string.login_no_input)
            loginPassword.error = ""
            loginEmail.requestFocus()
            return
        }else if (loginEmail.text.toString() == "") {
            loginEmail.error = getString(R.string.login_no_email)
            loginEmail.requestFocus()
            return
        }else if (loginPassword.text.toString() == "")  {
            loginPassword.error = getString(R.string.login_no_password)
            loginPassword.requestFocus()
            return
        }

        //Signing in with mAuth
        mAuth.signInWithEmailAndPassword(loginEmail.text.toString(), loginPassword.text.toString())
            .addOnCompleteListener(this)
            {task ->
                if(task.isSuccessful)   {
                    //Letting the app know that we're logged in
                    loggedIn = true

                    FunctionHelper.getAndSetUserData(applicationContext)

                    //Storing the access token
                    val token = mAuth.currentUser?.getIdToken(true).toString()
                    val sharedPreferences = getSharedPreferences("currentuser", Context.MODE_PRIVATE)
                    val editor = sharedPreferences.edit()
                    editor.putString("token", token)
                    editor.apply()
                    //Starting main


                    if (MainActivity.mainRunning)   {
                        finish()
                    }else   {
                        val newIntent = Intent(this, MainActivity::class.java)
                        startActivity(newIntent)
                        finish()
                    }

                }else   {
                    loginPassword.error = getString(R.string.login_password_invalid)
                    FunctionHelper.hideKeyboard(this)
                }
            }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean{
        menuInflater.inflate(R.menu.skip_bar, menu)
        return true
    }

    fun onSkip(item: android.view.MenuItem) {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    fun onSignup(view:View) {
        val intent = Intent(this, CreateAccount::class.java)
        startActivity(intent)
        finish()
    }
}

