package com.sambor.libtable.ui.activities

import android.app.AlertDialog
import android.content.Intent
import android.media.Image
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.kieronquinn.monetcompat.core.MonetCompat
import com.kieronquinn.monetcompat.view.MonetToolbar
import com.sambor.libtable.R
import com.sambor.libtable.kotlinback.FunctionHelper
import com.sambor.libtable.kotlinback.UserReviews
import kotlinx.android.synthetic.main.fragment_more.*
import android.content.DialogInterface
import android.widget.LinearLayout
import com.google.firebase.storage.StorageReference


class NewReview : AppCompatActivity() {

    lateinit var reviewTitle: EditText
    lateinit var reviewBody: EditText
    lateinit var restId: String
    lateinit var revImage1 : ImageView
    lateinit var revImage2 : ImageView
    lateinit var revImage3 : ImageView

    lateinit var mAuth: FirebaseAuth
    lateinit var mDatabase: FirebaseDatabase
    lateinit var mStorage: FirebaseStorage
    lateinit var mStorageReference: StorageReference

    var image1Changed = false
    var image2Changed = false
    var image3Changed = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val monetInstance = MonetCompat.getInstance()
        setContentView(R.layout.activity_new_review)
        val permToolbar = findViewById<MonetToolbar>(R.id.permToolbar)
        reviewTitle = findViewById<EditText>(R.id.reviewTitle)
        reviewBody = findViewById<EditText>(R.id.reviewBody)
        restId = intent.getStringExtra("id").toString()

        revImage1 = findViewById(R.id.revImage1)
        revImage2 = findViewById(R.id.revImage2)
        revImage3 = findViewById(R.id.revImage3)
        val newReviewView = findViewById<LinearLayout>(R.id.newReviewView)
        newReviewView.setBackgroundColor(monetInstance.getBackgroundColor(this, FunctionHelper.isDarkMode(this)))

        setSupportActionBar(permToolbar)
        val toolTitle = getString(R.string.review_toolbar_name) + " ${intent.getStringExtra("name")}"

        supportActionBar!!.title = toolTitle
        window.statusBarColor = MonetCompat.getInstance().getBackgroundColorSecondary(applicationContext, FunctionHelper.isDarkMode(applicationContext))!!
        permToolbar.setBackgroundColor(MonetCompat.getInstance().getBackgroundColorSecondary(applicationContext, FunctionHelper.isDarkMode(applicationContext))!!)


        mAuth = FirebaseAuth.getInstance()
        mDatabase = FirebaseDatabase.getInstance("https://libtable-default-rtdb.europe-west1.firebasedatabase.app")
        mStorage = FirebaseStorage.getInstance()
        mStorageReference = mStorage.reference

    }


    override fun onCreateOptionsMenu(menu : Menu): Boolean {
        menuInflater.inflate(R.menu.review_submit_bar, menu)
        return true
    }

    fun onSubmit(item: android.view.MenuItem)   {
        if (reviewTitle.text.toString() == "" && reviewBody.text.toString() == "") {
            reviewTitle.error = getString(R.string.review_error_all)
            reviewBody.error = ""
            return
        }else if (reviewTitle.text.toString() == "")    {
            reviewTitle.error = getString(R.string.review_error_title)
            return
        }else if (reviewBody.text.toString() == "")    {
            reviewBody.error = getString(R.string.review_error_body)
            return
        }else if (!image1Changed && !image2Changed && !image3Changed)   {
            var yesNo = true
            val dialogClickListener =
                DialogInterface.OnClickListener { dialog, which ->
                    when (which) {
                        DialogInterface.BUTTON_POSITIVE -> {
                            //just keep going
                        }
                        DialogInterface.BUTTON_NEGATIVE -> {
                            yesNo = false
                        }
                    }
                }

            val builder: AlertDialog.Builder = AlertDialog.Builder(applicationContext)
            builder.setMessage(getString(R.string.review_no_images)).setPositiveButton(getString(R.string.yes), dialogClickListener)
                .setNegativeButton(getString(R.string.no), dialogClickListener).show()
            if (!yesNo) {
                return
            }
        }



        val userReview = UserReviews(mAuth.currentUser!!.uid,getRandomString(10), reviewTitle.text.toString(), reviewBody.text.toString())


        mDatabase.getReference("Reviews").child(restId).child(userReview.reviewId).setValue(userReview).addOnCompleteListener(this){task->
            if (task.isSuccessful)  {


                if (image1Changed)  {
                    val imageRef = mStorageReference.child("images/reviews/${userReview.reviewId}/image1")
                    imageRef.putFile(Uri.parse(revImage1.tag.toString()))
                }

                if (image2Changed)  {
                    val imageRef = mStorageReference.child("images/reviews/${userReview.reviewId}/image2")
                    imageRef.putFile(Uri.parse(revImage2.tag.toString()))
                }

                if (image3Changed)  {
                    val imageRef = mStorageReference.child("images/reviews/${userReview.reviewId}/image3")
                    imageRef.putFile(Uri.parse(revImage3.tag.toString()))
                }



                Toast.makeText(applicationContext, getString(R.string.review_success), Toast.LENGTH_LONG).show()
                finish()
            }else   {
                Toast.makeText(applicationContext, getString(R.string.firebase_no_connect), Toast.LENGTH_LONG).show()
            }
        }
    }

    //This could do with some validation checks to make sure there aren't any repeats, but for the purposes of this it's
    //Not really necesarry
    private fun getRandomString(length: Int) : String {
        val allowedChars = ('A'..'Z') + ('a'..'z') + ('0'..'9')
        return (1..length)
            .map { allowedChars.random() }
            .joinToString("")
    }

    fun onImage1(view: View)    {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(intent, 1)
    }

    fun onImage2(view: View) {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(intent, 2)
    }

    fun onImage3(view:View) {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(intent, 3)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)  {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            if(requestCode ==1 && resultCode == AppCompatActivity.RESULT_OK && data.data != null) {
                val imageURI = data.data
                revImage1.setImageURI(imageURI)
                revImage1.tag = imageURI
                image1Changed = true
            }else if(requestCode ==2 && resultCode == AppCompatActivity.RESULT_OK && data.data != null) {
                val imageURI = data.data
                revImage2.setImageURI(imageURI)
                revImage2.tag = imageURI
                image2Changed = true
            }else if(requestCode ==3 && resultCode == AppCompatActivity.RESULT_OK && data.data != null) {
                val imageURI = data.data
                revImage3.setImageURI(imageURI)
                revImage3.tag = imageURI
                image3Changed = true
            }
        }
    }
}