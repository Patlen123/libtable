package com.sambor.libtable.ui.activities

import android.content.Context
import android.os.Bundle
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.google.firebase.auth.FirebaseAuth
import com.kieronquinn.monetcompat.app.MonetCompatActivity
import com.kieronquinn.monetcompat.core.MonetCompat
import com.kieronquinn.monetcompat.extensions.views.applyMonetRecursively
import com.sambor.libtable.R
import com.sambor.libtable.databinding.ActivityMainBinding
import com.sambor.libtable.kotlinback.FunctionHelper

class MainActivity : MonetCompatActivity() {

    private lateinit var binding: ActivityMainBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val navView: BottomNavigationView = binding.navView

        val navController = findNavController(R.id.nav_host_fragment_activity_main)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        //        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
        navView.applyMonetRecursively()
        navView.setBackgroundColor(MonetCompat.getInstance().getBackgroundColorSecondary(applicationContext)!!)
        window.statusBarColor = MonetCompat.getInstance().getBackgroundColorSecondary(applicationContext, FunctionHelper.isDarkMode(applicationContext))!!

        val sharedPreferences = getSharedPreferences("currentuser", Context.MODE_PRIVATE)
        val loggedInToken = sharedPreferences.getString("token", "null")

        mainRunning = true

    }

    override fun onDestroy()    {
        super.onDestroy()
        mainRunning = false
    }


    //Um this is the only way I could figure to have login go back to the correct fragment without making it hideously complicated
    companion object    {
        var mainRunning = false

    }
}